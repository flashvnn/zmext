<?php

/**
* ZMForm helper public static function for Drupal
*/
class ZMForm{
	public static function textfield($title, $default_value = NULL, $required = FALSE, $size = 30) {
	  $item = array(
			'#type'          => 'textfield',
			'#default_value' => $default_value,
			'#size'          => $size,
			'#required'      => $required,
	  );
	  $item['#title'] = $title;
	  return $item;
	}

	public static function textarea($title, $default_value = NULL, $required = FALSE, $rows = 5, $cols = 30, $resizeable = TRUE) {
	  return array(
			'#type'          => 'textarea',
			'#title'         => $title,
			'#default_value' => $default_value,
			'#rows'          => $rows,
			'#cols'          => $cols,
			'#resizeable'    => $resizeable,
			'#required'      => $required,
	  );
	}

	public static function texteditor($title, $default_value = NULL, $format = 'full_html', $required = FALSE, $rows = 5, $cols = 30, $resizeable = TRUE) {
		return array(
			'#type'          => 'text_format',
			'#title'         => $title,
			'#default_value' => $default_value,
			'#rows'          => $rows,
			'#cols'          => $cols,
			'#resizeable'    => $resizeable,
			'#required'      => $required,
			'#format'        => $format
		);
	}

	public static function select($title, $options = array(), $default_value = NULL, $required = FALSE) {
	  return array(
			'#type'          => 'select',
			'#title'         => $title,
			'#options'       => $options,
			'#default_value' => $default_value,
			'#required'      => $required,
	  );
	}

	public static function radios($title, $options = array(), $default_value = NULL, $required = FALSE) {
	  return array(
			'#type'          => 'radios',
			'#title'         => $title,
			'#options'       => $options,
			'#default_value' => $default_value,
			'#required'      => $required,
	  );
	}

	public static function checkboxes($title, $options = array(), $default_value = NULL, $required = FALSE) {
	  return array(
			'#type'          => 'checkboxes',
			'#title'         => $title,
			'#options'       => $options,
			'#default_value' => $default_value,
			'#required'      => $required,
	  );
	}

	public static function checkbox($title, $default_value = NULL, $required = FALSE) {
	  return array(
			'#type'          => 'checkbox',
			'#title'         => $title,
			'#default_value' => $default_value,
			'#required'      => $required,
	  );
	}
}