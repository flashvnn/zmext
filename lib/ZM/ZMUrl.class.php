<?php

/**
* ZMUrl helper functions
*/
class ZMUrl{
	/**
	 * Get full path form path
	 * @param  string $path    path want to create url
	 * @param  array  $options Drupal url options
	 * @return string
	 */
	public static function full($path, $options = array()){
		$options += array('absolute' => true);
		return url($path, $options);
	}
	/**
	 * Clone drupal url function
	 * @param  string $path
	 * @param  array  $options
	 * @return string
	 */
	public static function url($path, $options = array()){
		return url($path, $options);
	}
	/**
	 * Get home url
	 * @param  boolean $full
	 * @param  array   $options
	 * @return string
	 */
	public static function home($full = TRUE, $options = array()){
		if($full){
			$options += array('absolute' => true);
			return url("<front>", $options);
		}
		return url("<front>",$options);
	}
	/**
	 * Get current url
	 * @return string
	 */
	public static function current(){
		if (empty($_SERVER['HTTPS'])) {
				return "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		}
		return "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
	}

	public static function parent_url($url){
	  $data = drupal_parse_url($url);
	  $data_path = explode("/",rtrim($data['path'], '/'));
	  array_pop($data_path);
	  $data["path"] = implode("/", $data_path);
	  $data["absolute"]  = true;
	  return url($data["path"], $data);
	}


}
