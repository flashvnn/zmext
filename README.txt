ZM Extension

** Description
ZM Extension helper functions

[*] ZMEntityQuery
This class folk some code from module EntityFieldQuery Extra Fields at
https://drupal.org/project/efq_extra_field.

ZMEntityQuery is an extend of EntityFieldQuery. It support return extend data
direct from query make by EntityFieldQuery, you dont need use more query to get
data.

ZMEntityQuery also provided function to use OR condition width EntityFieldQuery.

Example code:

$query = new ZMEntityQuery();
  $result = $query->entityCondition('entity_type', 'node')
  ->propertyCondition('type', 'my_bundle_type')
  ->propertyCondition('status', 1)
  ->addExtraField('field_myfield', value', value')
  ->addExtraField('field_mynodereffield', 'nid', nid')
  ->addExtraField('', 'title', 'title', 'node')
  ->fieldCondition('field_myfield', 'value', 'some_value_to_filter_on', '=')
  ->execute();

Possible combinations :
addExtraField($field_name, $column, $column_alias = NULL, $table = NULL)

field_name

Specify from which field the extra field should come. If it is from a base table, leave it empty and fill in the table argument
column

Specify the name of the column,
column_alias

Specify the alias of the column,
table

Optionally, add something like node or user here (the base table),


Using FieldCondition with Or:

  $orImg = new ZMEntityFieldOr();
  $orImg->fieldCondition('field_image', 'fid', 100, '>');
  $orImg->fieldCondition('field_image', 'fid', 10, '<');

  $query = new ZMEntityQuery();
  $result = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'page')
    ->propertyCondition('status', 1)
    ->fieldConditionOr($orImg)
    ->preExecuteQuery(
      function (SelectQuery $selectQuery) {
        dpq($selectQuery);
      }
    )
    ->execute();

  if (!empty($result['node'])) {
    dpm($result['node']);
  }

Just create ZMEntityFieldOr instance and add field as use with EntityFieldQuery.

ZMEntityQuery also support anonymous function preExecuteQuery support you can
modify query before it execute, you can handle SelectQuery $selectQuery and do
what you want.
